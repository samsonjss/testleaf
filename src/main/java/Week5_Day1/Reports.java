package Week5_Day1;

import java.io.IOException;

import org.apache.commons.codec.digest.MessageDigestAlgorithms;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.MediaEntityModelProvider;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class Reports {
	

	ExtentHtmlReporter html;
	ExtentReports extent;
	ExtentTest test;
	@Test
	public void runreport() {
		//create a html template
		html = new ExtentHtmlReporter("./report/extentReport.html"); 
		extent = new ExtentReports();
		
		//fetching and to display all tescase
		html.setAppendExisting(true);
		
		extent.attachReporter(html);
		test = extent.createTest("TC001_Login","LOGIN INTO TESTLEAF");
		test.assignAuthor("SAM");
		test.assignCategory("SMOKE");
		
		try {
			test.pass("USERNAME ENTERED SUCCESSFULLY", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		extent.flush();
		
		
		
		
	}

}
