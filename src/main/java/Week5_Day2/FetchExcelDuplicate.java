package Week5_Day2;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class FetchExcelDuplicate {

	static Object [][] data;

	public static Object[][] readData (String dataSheetName)  throws IOException{
		
		
		
		XSSFWorkbook wbook;
		
		//Enter into workbook
				
					 wbook = new XSSFWorkbook("./EXCEL/"+dataSheetName+".xlsx");
					
					//Enter into sheet
					XSSFSheet sheet = wbook.getSheet("Sheet1");
					
					//Getting Rowcount
					int rowcount = sheet.getLastRowNum();
					System.out.println("ROwcount"+rowcount);
					
					//Getting cell count for header
					short colcount = sheet.getRow(0).getLastCellNum();
					System.out.println("Columncount" +colcount);
					
					data = new Object[rowcount][colcount];
					
					for(int i = 1;i <=rowcount;i++) {
						//entering row
						XSSFRow row = sheet.getRow(1);
						
						for (int j = 0; j < colcount; j++) {
							//Enter cell
							XSSFCell cell = row.getCell(j);
							//getting value from cell
							//String text = cell.getStringCellValue();
							//System.out.println(text);
							data[i-1][j]= cell.getStringCellValue();
							
						}
					}
			
		return data;
				
	}
}