package Week5_Day2;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class FetchExcel {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		//Enter into workbook
		XSSFWorkbook wbook = new XSSFWorkbook("./EXCEL/TCOO1.xlsx");
		
		//Enter into sheet
		XSSFSheet sheet = wbook.getSheet("Sheet1");
		
		//Getting Rowcount
		int rowcount = sheet.getLastRowNum();
		System.out.println("ROwcount"+rowcount);
		
		//Getting cell count for header
		short colcount = sheet.getRow(0).getLastCellNum();
		System.out.println("Columncount" +colcount);
		
		for(int i = 1;i <=rowcount;i++) {
			//entering row
			XSSFRow row = sheet.getRow(1);
			
			for (int j = 0; j < colcount; j++) {
				//Enter cell
				XSSFCell cell = row.getCell(j);
				//getting value from cell
				String text = cell.getStringCellValue();
				System.out.println(text);
				
				wbook.close();
				
			}
		}

	}

}
