package Week5_Day2;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;

public class ZoomCar  {

	@Test
	public void testZoomCar() throws InterruptedException {
		
		
		//1. Open Chrome Browser

		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");

		ChromeDriver driver= new ChromeDriver();

		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(4000, TimeUnit.SECONDS);
		
		//2. Open https://www.zoomcar.com/chennai Website

		driver.get("https://www.zoomcar.com/chennai/");

		//3. Click on the Start your wonderful journey link
		
		driver.findElementByXPath("//a[@class='search']").click();

		//4. In the Search page, Click on the any of the pick up point under POPULAR PICK-UP POINTS
		
		driver.findElementByXPath("//div[@class='items'][2]").click();

        //5. Click on the Next button
		
		driver.findElementByXPath("//button[@class='proceed']").click();

		//6. Specify the Start Date as tomorrow Date
		// Get the current date

		Date date = new Date();

		//Getonlythedate("andnotmonth", "year", "timeetc");

		DateFormat sdf = new SimpleDateFormat("dd");

		// Get today's date

		String today = sdf.format(date);

		// Convert to integer and add 1 to it

		int tomorrow = Integer.parseInt(today)+1;

		// Print tomorrow's date

		System.out.println(tomorrow);

		driver.findElement(By.xpath("//div[contains(text(),'"+tomorrow+"')]")).click();

		
		//7. Click on the Next Button
		
		driver.findElement(By.xpath("//button[@class='proceed']")).click();
		
		
		//8. Confirm the Start Date and Click on the Done button
		driver.findElement(By.xpath("//button[@class='proceed']")).click();
		
		
		//9. In the result page, capture the number of results displayed.
		 List<String> ls = new ArrayList<>();
		 
		 List<WebElement> elementsByXPath = driver.findElementsByXPath("//div[@class='price']");
		 
		 for (WebElement webElement : elementsByXPath) {
			ls.add(webElement.getText().replaceAll("\\D", ""));
			
		}
		 
		System.err.println(ls);
		
		String max = Collections.max(ls);
		System.out.println(max);
		//System.out.println(ls.indexOf(Integer.parseInt(max)));
		
		
		
		//click(locateElement("xpath", "//div[contains(text(),'329')]/../button[@name='book-now']"));
		//String text = driver.findElementByXpath("//div[@class='details'],'"+max+"')/h1").getText();
		
		//10. Find the highest value and report the brand name.
		String text = driver.findElementByXPath("//div[contains(text(),'"+max+"')]/parent::div/parent::div/preceding-sibling::div[1]/h3").getText();
		
		System.out.println(text);
		
		//11. click on the Book Now button for it.
		
	    driver.findElement(By.xpath("//div[contains(text(),'"+max+"')]/../button[@name='book-now']")).click();
	    
	    //System.out.println(findElement);
	    
	    //findElement.click();
		
	    
	   // String textBN = driver.findElementByXPath("//div[contains(text(),'"+max+"')]/parent::div/parent::div/preceding-sibling::div[1]/h3").getText();
       //// System.out.println(textBN);
       // driver.findElementByXPath("//div[contains(text(),'"+max+"')]/following-sibling::div//following-sibling::button").click();
       // driver.close();
	   
	    //12. Close the Browser.
	    driver.close();
	
		


	}

}
