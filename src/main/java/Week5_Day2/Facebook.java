package Week5_Day2;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class Facebook {
	
	@Test
	public void  testFacebook() {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		
		ChromeDriver driver = new ChromeDriver();
		
		driver.get("https://www.facebook.com/");
		
		driver.manage().window().maximize();
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		driver.findElementByXPath(" //input[@id='email']").sendKeys("9952554636");
		
		driver.findElementByXPath(" //input[@id='pass']").sendKeys("ramolasam");
		
		driver.findElementByXPath(" //input[@type='submit']").click();
		
		driver.findElementByXPath("//div[@id='u_q_2']/input[2]").sendKeys("TestLeaf");
		
		driver.findElementByXPath("//div[@id='u_q_1']/preceding-sibling::button/i").click();
		
		driver.close();
		
		
	}

}
