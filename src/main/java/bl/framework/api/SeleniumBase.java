package bl.framework.api;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import Week5_Day1.AdvanceReport;
import bl.framework.base.Browser;
import bl.framework.base.Element;

//mutiple inheritance concept here use
public class SeleniumBase  extends AdvanceReport implements Browser, Element {

	public RemoteWebDriver driver;
	public int i = 1;
	public String text = null;
	public String text1 = null;
	public String title = null;

	@Override
	public void startApp(String url) {
		// TODO Auto-generated method stub

	}

	@Override
	public void startApp(String browser, String url) {
		try {
			if (browser.equalsIgnoreCase("chrome")) {
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				driver = new ChromeDriver();
			} else if (browser.equalsIgnoreCase("firefox")) {
				System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
				driver = new FirefoxDriver();
			}
			driver.manage().window().maximize();
			driver.manage().deleteAllCookies();
			driver.get(url);
			driver.manage().timeouts().pageLoadTimeout(40,TimeUnit.SECONDS);
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			System.out.println("The browser " + browser + " launched successfully");
			takeSnap();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("WebDriver Exception Occured");

		} finally {
			takeSnap();
		}
	}

	@Override
	public WebElement locateElement(String locatorType, String value) {
		try {
			switch (locatorType) {
			case "id":
				return driver.findElementById(value);
			case "name":
				return driver.findElementByName(value);
			case "class":
				return driver.findElementByClassName(value);
			case "xpath":
				return driver.findElementByXPath(value);
			case "LinkText":
				return driver.findElementByLinkText(value);
			case "PartialLinkText":
				return driver.findElementByPartialLinkText(value);
			default:
				break;
			}
		} catch (NotFoundException e) {
			System.err.println("The element value " + value + "not found");
		} catch (WebDriverException e) {
			System.err.println("WebDriverException occured");
		}
		return null;
	}

	@Override
	public WebElement locateElement(String value) {
		// TODO Auto-generated method stub
		try {
			 driver.findElementById(value);
		} catch (NoSuchElementException e) {
			System.err.println("locator value is incorrect or Not found");
		}catch (RuntimeException e) {
			System.err.println("Something went wrong while locating the element");
		}
		return null;
	}

	@Override
	public List<WebElement> locateElements(String type, String value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void switchToAlert() {
		// TODO Auto-generated method stub
		try {
			driver.switchTo().alert();
		} catch (NoAlertPresentException e) {
			System.err.println("There is no alert to handle at this moment");
		} catch (RuntimeException e) {
			System.err.println("Something went wrong while locating the element");
		}
	}

	@Override
	public void acceptAlert() {
		// TODO Auto-generated method stub
		try {
			driver.switchTo().alert().accept();
		} catch (NoAlertPresentException e) {
			System.err.println("There is no alert to handle at this moment");
		} catch (RuntimeException e) {
			System.err.println("Something went wrong while locating the element");
		}
	}

	@Override
	public void dismissAlert() {
		// TODO Auto-generated method stub
		try {
			driver.switchTo().alert().dismiss();
		} catch (NoAlertPresentException e) {
			System.err.println("There is no alert to handle at this moment");
		} catch (RuntimeException e) {
			System.err.println("Something went wrong while locating the element");
		}
	}

	@Override
	public String getAlertText() {
		// TODO Auto-generated method stub
		try {
			driver.switchTo().alert().getText();
		} catch (NoAlertPresentException e) {
			System.err.println("There is no alert to handle at this moment");
		} catch (RuntimeException e) {
			System.err.println("Something went wrong while locating the element");
		}
		return null;
	}

	@Override
	public void typeAlert(String data) {
		// TODO Auto-generated method stub
		try {
			driver.switchTo().alert().sendKeys(data);
		} catch (NoAlertPresentException e) {
			System.err.println("There is no alert to handle at this moment");
		} catch (RuntimeException e) {
			System.err.println("Something went wrong while locating the element");
		}

	}

	@Override
	public void switchToWindow(int index) {
		// TODO Auto-generated method stub
		try {
		Set<String> allwindows1 = driver.getWindowHandles();
		List<String> ls1 = new ArrayList(allwindows1);
		driver.switchTo().window(ls1.get(index));

	}catch (IndexOutOfBoundsException e) {
		System.err.println("index not found");
	}catch (NoSuchWindowException e) {
		System.err.println("no such window found or it might have already closed");
	}catch (RuntimeException e) {
		System.err.println("Something went wrong while locating the window");
	}
	finally {
		takeSnap();
	}
	}

	@Override
	public void switchToWindow(String title) {
		// TODO Auto-generated method stub

	}

	@Override
	public void switchToFrame(int index) {
		// TODO Auto-generated method stub

	}

	@Override
	public void switchToFrame(WebElement ele) {
		// TODO Auto-generated method stub

	}

	@Override
	public void switchToFrame(String idOrName) {
		// TODO Auto-generated method stub

	}

	@Override
	public void defaultContent() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean verifyUrl(String url) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean verifyTitle(String title) {
		// TODO Auto-generated method stub
		try {
			title = driver.getTitle();
			if(title.contains(title)) 
			{
				System.out.println("Verified the title has: "+title); 
				return true;
			}
				
			else
			{
				System.out.println("Veified the title is not having : "+title); 
			    return false;
			}
		} catch (WebDriverException e) {
			System.err.println("Something went wrong while verifying the title");
		} catch (RuntimeException e) {
			System.err.println("Something went wrong while verifying the title");
		}

		return false;
	}

	@Override
	public void takeSnap() {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File des = new File("./snaps/img" + i + ".png");
		try {
			FileUtils.copyFile(src, des);
		} catch (IOException e) {
			e.printStackTrace();
		}
		i++;
	}

	@Override
	public void close() {
		// TODO Auto-generated method stub
		
		driver.close();

	}

	@Override
	public void quit() {
		// TODO Auto-generated method stub
      driver.quit();
	}

	@Override
	public void click(WebElement ele) {
		try {
			ele.click();
			System.out.println("The element "+ele+" clicked successfully");
		} catch (NullPointerException e) {
			System.err.println("web element is Null");
		}catch (NoSuchElementException e) {
			System.err.println("web element is incorrect or not found");
		}catch (RuntimeException e) {
			System.err.println("Something went wrong with the element");
		}
		
	}

	@Override
	public void append(WebElement ele, String data) {
		// TODO Auto-generated method stub
		try {
			ele.sendKeys(data);
		} catch (NullPointerException e) {
			System.err.println("web element is Null");
		}catch (NoSuchElementException e) {
			System.err.println("web element is incorrect or not found");
		}catch (RuntimeException e) {
			System.err.println("Something went wrong with the element");
		}
		finally {
			takeSnap();
		}
		

	}

	@Override
	public void clear(WebElement ele) {
		// TODO Auto-generated method stub
		try {
			ele.clear();
		} catch (NullPointerException e) {
			System.err.println("web element is Null");
		}catch (NoSuchElementException e) {
			System.err.println("web element is incorrect or not found");
		}catch (RuntimeException e) {
			System.err.println("Something went wrong with the element");
		}
		finally {
			takeSnap();
		}		

	}

	@Override
	public void clearAndType(WebElement ele, String data) {
		try {
			ele.clear();
			ele.sendKeys(data);
			System.out.println("The data " + data + " entered successfully");
		} catch (WebDriverException e) {
			System.err.println("Unknown Exception");
		} finally {
			takeSnap();
		}
	}

	@Override
	public String getElementText(WebElement ele) {
		// TODO Auto-generated method stub
		 try {
				text1 = ele.getText();
				System.out.println(text1);
				return text1;
			} catch (NullPointerException e) {
				System.err.println("web element is Null");
			}catch (NoSuchElementException e) {
				System.err.println("web element is incorrect or not found");
			}catch (RuntimeException e) {
				System.err.println("Something went wrong with the element");
			}
		return null;
	}

	@Override
	public String getBackgroundColor(WebElement ele) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getTypedText(WebElement ele) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		// TODO Auto-generated method stub
		try {
			Select sc = new Select(ele);
			List<WebElement> options = sc.getOptions();
			for (WebElement webElement : options) {
				if(webElement.getText().startsWith(value)) {
					sc.selectByIndex(options.indexOf(webElement));
					break;
				}
			}
		}  catch (NullPointerException e) {
			System.err.println("Web element is Null");	
		}catch (NoSuchElementException e) {
			System.err.println("element is incorrect or not found");
		}catch (StaleElementReferenceException e) {
			System.err.println("The element you are looking for is not found in the dropldown list");
		}catch (RuntimeException e) {
			System.err.println("Something went wrong with the element");
		}
	    finally {
	    	takeSnap();
	    }

	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		// TODO Auto-generated method stub

		try {
			Select sdi = new Select(ele);
			sdi.selectByIndex(index);
		} catch (NullPointerException e) {
			System.err.println("Web element is Null");
		} catch (NoSuchElementException e) {
			System.err.println("element is incorrect or not found");
		} catch (StaleElementReferenceException e) {
			System.err.println("The element you are looking for is not found in the dropldown list");
		} catch (RuntimeException e) {
			System.err.println("Something went wrong with the element");
		} finally {
			takeSnap();
		}

	}

	@Override
	public void selectDropDownUsingValue(WebElement ele, String value) {
		// TODO Auto-generated method stub

		try {
			Select sdi = new Select(ele);
			sdi.selectByValue(value);
		} catch (NullPointerException e) {
			System.err.println("Web element is Null");
		} catch (NoSuchElementException e) {
			System.err.println("element is incorrect or not found");
		} catch (StaleElementReferenceException e) {
			System.err.println("The element you are looking for is not found in the dropldown list");
		} catch (RuntimeException e) {
			System.err.println("Something went wrong with the element");
		} finally {
			takeSnap();
		}

	}

	@Override
	public boolean verifyExactText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		try {
			text = ele.getText();
			if (text.equalsIgnoreCase(expectedText)) {
				System.out.println("Yes:  " + text + " is same as expected : " + expectedText);
				return true;
			} else {
				System.out.println("No: " + text + " is not same as expected : " + expectedText);
				return false;
			}
		} catch (NullPointerException e) {
			System.err.println("Web element is Null");
		} catch (NoSuchElementException e) {
			System.err.println("element is incorrect or not found");
		} catch (RuntimeException e) {
			System.err.println("Something went wrong with the element");
		} finally {
			takeSnap();
		}
		return false;
	}

	@Override
	public boolean verifyPartialText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		try {
			text = ele.getText();
			if (text.contains(expectedText)) {
				System.out.println("Actual Text: " + text);
				return true;
			} else {
				System.out.println("Expacted Text is: " + expectedText + " Actual Text is: " + text);
				return false;
			}
		} catch (NullPointerException e) {
			System.err.println("Web element is Null");
		} catch (NoSuchElementException e) {
			System.err.println("element is incorrect or not found");
		} catch (RuntimeException e) {
			System.err.println("Something went wrong with the element");
		} finally {
			takeSnap();
		}
		return false;
	}

	@Override
	public boolean verifyExactAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean verifyDisplayed(WebElement ele) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean verifyDisappeared(WebElement ele) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean verifyEnabled(WebElement ele) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean verifySelected(WebElement ele) {
		// TODO Auto-generated method stub
		return false;
	}

}
