package bl.framework.testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;

public class TC004_MergeLead extends SeleniumBase{
	
	@Test(groups="Sanity")
	public void mergelead() throws InterruptedException {
		
		
		//LAUNCH BROWSER
		startApp("chrome", "http://leaftaps.com/opentaps");
		
		//ENTER THE USERNAME
		//WebElement eleUsername = locateElement("id", "username");
		//clearAndType(eleUsername, "DemoSalesManager"); 
		
		clearAndType(locateElement("id", "username"),"DemoSalesManager");
		
		//ENTER THE PASSWORD
		
		clearAndType (locateElement("id", "password"),"crmsfa");
		
	    
		
		//CLICK LOGIN
		//WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(locateElement("class", "decorativeSubmit")); 
		
		
		
		//click CRMFA
				
				click(locateElement("LinkText","CRM/SFA"));
				
		//CLICK LEADS LINK
				
				click(locateElement("xpath", "//a[text()='Leads']"));
				
		//click Merge Leads
				
				click(locateElement("xpath","//a[text()='Merge Leads']"));
				
		//click on icon near from Lead
				
				click(locateElement("xpath", "//table[@id='widget_ComboBox_partyIdFrom']//following::a[1]"));
				
		//move to new window
				
				switchToWindow(1);
				
		//enter first name
			  WebElement firstname = locateElement("name", "firstName");
			  clearAndType(firstname,"SAMSON");
			  
			  
	  //click FindLeads button
			  click(locateElement("xpath", "//button[text()='Find Leads']"));
			  Thread.sleep(3000);
			  
		//click first resulting lead
			  
			  click(locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a"));
			  //System.out.println("windows entere");
		
			  //switch back to primary windo
			  switchToWindow(0);
			  
			  
			  
	  //click on icon near to Lead
			 
				click(locateElement("xpath", "//table[@id='widget_ComboBox_partyIdTo']//following::a[1]"));
				
		//15--move to new window	
				
				switchToWindow(1);
	          
		//16--enter lead id
				
				clearAndType(locateElement("name", "id"),"10105");
				
				
		//17-click Find Lead Button
				
				WebElement findlead = locateElement("xpath", "//button[text()='Find Leads']");
				
				click(findlead);
				
				Thread.sleep(1000);
				
		//18--click first resulting Lead
				
				click(locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a"));
			
		//19 switch back to primary window
				
				switchToWindow(0);
				
				
		//20 click merge
				
				click(locateElement( "xpath", "//a[@class='buttonDangerous']"));
				
		//21 Accept Alert
				
				driver.switchTo().alert().accept();
				
		//22 CLICK FIND LEADS
				
				
				WebElement findleads1 = locateElement("linktext", "Find Leads");
				
				click(findleads1);
				
				Thread.sleep(3000);
				
				//verifyExactText(locateElement("xpath", "//div[@class='x-paging-info']"), "No records to display");
				
//		//23--ENTER FROM LEAD NAME
//				
//				WebElement Leadname = locateElement("xpath", "(//input[@name='firstName'])[3])");
//				
//				clearAndType(Leadname, "SAM");
//				
//		//24--click FindLeads  
//				
//              WebElement findleads2 = locateElement("linktext", "Find Leads");
//				
//				click(findleads2);
				
				
	//verify the error msg
				
		//close the browser 
				
				driver.close();
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
			
			  
			 
			  
			  
			  
			  
			  
			  
			  
			  
			  
			  
			  
			  
			  
			  
		
		
	}

}
