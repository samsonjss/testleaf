package bl.framework.testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;
import bl.framework.design.ProjectMethods;

public class TC001_LoginAndLogout extends ProjectMethods{

	@Test
	public void loginandlogout() {
				
				//CLICK CREATE LEAD
				WebElement createLead = locateElement("LinkText","Create Lead");
				click(createLead);
				
				WebElement compnyname = locateElement("id","createLeadForm_companyName");
				clearAndType(compnyname,"SIMEON");
				
				WebElement FirstName = locateElement("xpath","//input[@id='createLeadForm_firstName']");
				clearAndType(FirstName,"SAMSON");
				
				 WebElement LastName = locateElement("id","createLeadForm_lastName");
				 clearAndType(LastName, "SINGH");
				 
				 WebElement submit = locateElement("name","submitButton");
				 click(submit);
			
	}
}








