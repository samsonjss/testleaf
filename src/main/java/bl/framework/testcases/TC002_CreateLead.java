package bl.framework.testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import Week5_Day2.FetchExcelDuplicate;
import bl.framework.api.SeleniumBase;
import bl.framework.design.ProjectMethods;

public class TC002_CreateLead extends ProjectMethods {
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC002_CreateLead";
		testDec = "Create a new Lead in leaftaps";
		author = "SAMSON";
		category = "Smoke";
		datasheetname="TCOO1";
	} 
	
	@DataProvider(name="getdata",indices= 0)
	public Object[][] fetchdata() {
		
		
		

		Object[][] data = new String[2][3];
		data[0][0]="Testleaf";
		data[0][1]="SAM";
		data[0][2]="c";
		
		
		data[1][0]="Testleaf";
		data[1][1]="SIM";
		data[1][2]="D";
		
		return data;
		
		
		
	}
	
	@DataProvider(name="getdata1")
	public String[][] fetchdata1() {
		String[][] data = new String[2][3];
		data[0][0] = "TestLeaf";
		data[0][1] = "Subhu";
		data[0][2] = "C";
		
		data[1][0] = "TestLeaf";
		data[1][1] = "Ramola";
		data[1][2] = "C";
		return data;
	}
	
	//@Test(dataProvider="getData",invocationCount=1)
	@Test(dataProvider="getdata")
	//@Test(groups="smoke")
	//@Test(invocationCount=1,invocationTimeOut=80000)
	public void CreateLead(String cname,String fname,String lname) {
				
				//CLICK CREATE LEAD
				WebElement createLead = locateElement("LinkText","Create Lead");
				click(createLead);
				
				
				WebElement compnyname = locateElement("id","createLeadForm_companyName");
				clearAndType(compnyname,cname);
				
				WebElement FirstName = locateElement("xpath","//input[@id='createLeadForm_firstName']");
				clearAndType(FirstName,fname);
				
				 WebElement LastName = locateElement("id","createLeadForm_lastName");
				 clearAndType(LastName, lname);
				 
				 
				 
				//select dropdown
				 
				 selectDropDownUsingText(locateElement("id", "createLeadForm_dataSourceId"),"Conference");
				 
				 WebElement submit = locateElement("name","submitButton");
				 
				WebElement locallast = locateElement("id", "createLeadForm_lastNameLocal");
				
				clearAndType(locallast, "SAM");
				
				
		       selectDropDownUsingIndex(locateElement("id","createLeadForm_marketingCampaignId"), 1);
				
				 
				 click(submit);
				 
				// driver.close();
				 
				 
				 
		
		
	}

}
