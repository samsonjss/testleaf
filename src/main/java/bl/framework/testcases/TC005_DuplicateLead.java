package bl.framework.testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import bl.framework.design.ProjectMethods;

public class TC005_DuplicateLead extends ProjectMethods {
	
	@Test(groups="regression")
	public void CreateDupLead() {

		click(locateElement("LinkText", "Leads"));
		click(locateElement("LinkText", "Find Leads"));
		click(locateElement("xpath", "//span[text()='Email']"));
		clearAndType(locateElement("name", "emailAddress"), "samsonmca1990@gmail.com");
		click(locateElement("xpath", "//button[text()='Find Leads']"));
		WebElement elemnt1 = locateElement("xpath", "//td[@class='x-grid3-col x-grid3-cell x-grid3-td-firstName ']/div/a");
		String text = getElementText(elemnt1);
		click(elemnt1);
		//System.out.println("Page Title is: " +getTitle());
		click(locateElement("xpath", "//a[@class='subMenuButton']"));
		System.out.println(verifyTitle("Duplicate Lead"));
		click(locateElement("name", "submitButton"));
		boolean value = verifyExactText(locateElement("id", "viewLead_firstName_sp"), text);
		System.out.println("Verified the text and it is Duplicate or not : "+value);
		close();
	
	}

}
