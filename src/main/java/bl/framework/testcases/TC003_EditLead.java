package bl.framework.testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import bl.framework.api.SeleniumBase;


public class TC003_EditLead extends SeleniumBase {
	
	@Test(groups="smoke")
	//@Test(dependsOnMethods= {"bl.framework.testcases.TC002_CreateLead.CreateLead"})
	public void edit() {
		
		startApp("chrome", "http://leaftaps.com/opentaps");
		
		WebElement eleUsername = locateElement("id", "username");
		clearAndType(eleUsername, "DemoSalesManager"); 
		WebElement elePassword = locateElement("id", "password");
		clearAndType(elePassword, "crmsfa"); 
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin); 
		
		//click CRMFA
				WebElement ClickCRMFA  = locateElement("LinkText","CRM/SFA");
				click(ClickCRMFA);
				
		// click lead
				
				WebElement clickLead = locateElement("LinkText", "Leads");
				click(clickLead);
				
		//click find lead
				WebElement clickFndLead = locateElement("LinkText", "Find Leads");
				click(clickFndLead);
				
				
		//enter the name in First name field
				
				WebElement passfname = locateElement("xpath", "(//input[@name='firstName'])[3]");
				
				clearAndType(passfname, "SAMSON");
				
		//click FIND LEAD BUTTON
				
				WebElement clickFLEAD = locateElement("xpath", "//button[text()='Find Leads']");
				click(clickFLEAD);
				
				
		     //click particular record
				
				WebElement clickparrecord = locateElement("xpath",   "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
				click(clickparrecord);

	         // get and verify the Title
				
				verifyTitle("View Lead");
				
				
	       //click "Edit button"
				
				WebElement clickEdit = locateElement("xpath", "//a[text()='Edit']");
				click(clickEdit);
				
	       //update the company name
				
				WebElement updatename = locateElement("xpath", "//input[@id='updateLeadForm_companyName']");
				
				clearAndType(updatename, "AMAZON");
				
		//click submit button
		        
				WebElement submit = locateElement("xpath", "(//input[@name='submitButton' ])[1]");
				click(submit);
				
			driver.close();
	}

	
}
