package bl.framework.design;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import com.beust.jcommander.Parameter;

import Week5_Day1.AdvanceReport;
import Week5_Day2.FetchExcelDuplicate;
import bl.framework.api.SeleniumBase;




public class ProjectMethods extends  SeleniumBase {

	public String testcaseName, testDec, author, category,datasheetname;
	@BeforeSuite(groups = {"common"})
	public void beforeSuite() {
		startReport();
	}
	@AfterSuite(groups = {"common"})
	public void afterSuite() {
		endReport();
	}
	@BeforeClass(groups = {"common"})
	public void beforeClass() {
		initializeTest(testcaseName, testDec, author, category);
	}
	
	@Parameters({"url","username","password"})   
	@BeforeMethod
	public void login(String url,String usname,String pass) {
		/*startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUsername = locateElement("id", "username");
		clearAndType(eleUsername, "DemoSalesManager"); 
		WebElement elePassword = locateElement("id", "password");
		clearAndType(elePassword, "crmsfa"); 
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		click(locateElement("LinkText", "CRM/SFA")); */
		
		startApp("chrome", url);
		WebElement eleUsername = locateElement("id", "username");
		clearAndType(eleUsername, usname); 
		WebElement elePassword = locateElement("id", "password");
		clearAndType(elePassword, pass ); 
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		click(locateElement("LinkText", "CRM/SFA"));
		
		
		
		
		
	}
	
	@AfterMethod(enabled=false)
	public void closeApp() {
		close();
	}
	
	
	@DataProvider(name="fetchData")
	public  Object[][] getdata() throws IOException {
		
		return FetchExcelDuplicate.readData(datasheetname);
		
		
		
		
	}
}










